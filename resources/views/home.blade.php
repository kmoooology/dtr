@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">

                    <a class="btn btn-default" href={{ url('/employee') }} role="button">Show employees</a>
                    

                    <a class="btn btn-default" href={{ url('/time') }} role="button">Daily Time Review</a>

                    @can('Add User')
                        <a class="btn btn-default" href="{{ route('users.index') }}">Users </a>
                    @endcan

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
