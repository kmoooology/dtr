@extends('layouts.layouts')

@include('layouts.nav')
@section('title', 'Employee List')

@section('content')


	@if(Session::has('message'))
	<div class="container alert alert-success alert-dismissable">
		<h4 class="alert-heading">Success!</h4>
		{{Session::get('message')}}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  		<span aria-hidden="true">&times;</span>
		</button>
	</div>
	@endif

	<div class="container">
		<h3>List of Employee</h3>
		<button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal1">
            Add Employee
        </button>

		<table class="table table-hover">
			<thead>
				<tr>
					<td>Lastname</td>
					<td>Firstname</td>
					<td>Address</td>
					<td>Birthdate</td>
				</tr>
			</thead>
			<tbody>
				@foreach($employees as $employee)
					<tr>
						<td>{{ $employee->lastname }}</td>
						<td>{{ $employee->firstname }}</td>
						<td>{{ $employee->address }}</td>
						<td>{{ $employee->birthdate }}</td>
						<td>
							<div class="btn-group pull-right">
                          		<button class="edit-modal btn btn-success" data-toggle="modal" data-target="#editEmployee" data-id="{{$employee->id}}" data-barcode="{{$employee->barcode}}" data-lastname="{{$employee->lastname}}" data-firstname="{{$employee->firstname}}" data-address="{{$employee->address}}" data-birthdate="{{$employee->birthdate}}">
                            	<span class="glyphicon glyphicon-pencil"></span>
                          		</button>
                  			</div>
					</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<center>{{ $employees->links() }}</center>
	</div>
@endsection


@section('modal1')
@parent
<div id="editEmployee" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="panel panel-primary">
          <div class="panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="panel-title" id="myModalLabel"><b>Edit Employee</b></h4>
          </div>
        <div class="modal-body">
        
        	 @if(isset($employee->id))
            {!! Form::model($employee, ['route' => ['employee.update', $employee->id], 'method' => 'PUT']) !!}
            {!! Form::hidden('id', null, ['id' => 'uid', 'class' => 'form-control', 'required' => '']) !!}

            {!! Form::label('Barcode') !!}
            {!! Form::text('barcode', null, ['id' => 'bcode', 'class' => 'form-control', 'readonly']) !!}

            {!! Form::label('Lastname') !!}
			{!! Form::text('lastname', null, ['id' => 'lname', 'class' => 'form-control', 'required' => '', 'maxlength' => '20', 'autocomplete'=>'off']) !!}

			{!! Form::label('Firstname') !!}
			{!! Form::text('firstname', null, ['id' => 'fname', 'class' => 'form-control', 'required' => '', 'maxlength' => '20', 'autocomplete'=>'off']) !!}

			{!! Form::label('Address') !!}
			{!! Form::text('address', null, ['id' => 'uaddress', 'class' => 'form-control', 'required' => '', 'maxlength' => '100', 'autocomplete'=>'off']) !!}

			{!! Form::label('Birthdate') !!}
			{!! Form::date('birthdate', null, ['id' => 'ubirthdate', 'class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}



                <div class="modal-footer">
                  <button type="button" class="btn btn-primary" data-dismiss="modal" style="margin-top:20px">Cancel</button>
                  {!! Form::submit('Update', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}
                </div>
            	{!! Form::close() !!}
            	@endif
        </div>
      </div>
    </div>
</div>    

{{-- Add User Modal --}}
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Add Employee</h4>
	      </div>
	      <div class="modal-body">
	        {!! Form::open(['route' => 'employee.store', 'Method' => 'POST']) !!}

			{!! Form::label('Lastname') !!}
			{!! Form::text('lastname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20', 'autocomplete'=>'off']) !!}

			{!! Form::label('Firstname') !!}
			{!! Form::text('firstname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20', 'autocomplete'=>'off']) !!}

			{!! Form::label('Address') !!}
			{!! Form::text('address', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100', 'autocomplete'=>'off']) !!}

			{!! Form::label('Birthdate') !!}
			{!! Form::date('birthdate', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	        {!! form::submit('Save', ['class' => 'btn btn-success'])!!}

	      </div>
	      {!! Form::close() !!}
	    </div>
	  </div>
	</div>
@endsection

@section('scripts')
@parent
    {{-- Edit User Modal --}}
    <script type="text/javascript">
        $(document).on('click', '.edit-modal', function() {
            $('#uid').val($(this).data('id'));
            $('#lname').val($(this).data('lastname'));
            $('#fname').val($(this).data('firstname'));
            $('#uaddress').val($(this).data('address'));
            $('#ubirthdate').val($(this).data('birthdate'));

            $('#editEmployee').modal('show');
        });
    </script>
@endsection

