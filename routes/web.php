<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (\Illuminate\http\Request $request) {
	$user = $request->user();
	#dd($user->hasRole('Admin'));
	#dd($user->can('Add_user'));
    return view('welcome');
});

Route::resource('employee', 'EmployeeController');

Route::resource('time', 'TimeController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {
	Route::resource('employee', 'EmployeeController', ['except' => ['show', 'index']]);
});

Route::group(['middleware' => 'auth'], function() {
	Route::resource('users', 'UserController', ['except' => ['create', 'show', 'destroy']]);
	Route::post('updateroles', ['uses' => 'UserController@update_role', 'as' => 'users.update_role']);
});