<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeOut extends Model
{
    function employees(){
    	return $this->belongsTo(Employee::class, 'employees');
    }
}
