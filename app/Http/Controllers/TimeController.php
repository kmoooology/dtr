<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Employee;
use App\TimeIn;
use App\TimeOut;

class TimeController extends Controller
{
    //
    public function index() {
        $clockedin = TimeIn::leftJoin('employees', 'time_ins.emp_id', '=', 'employees.id')->select('employees.lastname', 'employees.firstname', 'time_ins.created_at')->where('time_ins.time_out', '=', '0')->whereDate('time_ins.created_at',DB::raw('CURDATE()'))->get();
        $clockedout = TimeOut::leftJoin('employees', 'time_outs.emp_id', '=', 'employees.id')->select('employees.lastname', 'employees.firstname', 'time_outs.created_at')->whereDate('time_outs.created_at',DB::raw('CURDATE()'))->get();
        $employee = Employee::all();

        return view('time.index', compact('clockedin','clockedout','employee'));
    }

    public function store(Request $request) {
        if (!Employee::whereBarcode($request -> barcode)->exists()) {
            return redirect()->route('time.index')->with('message', 'Employee Existence in Question');
        }
    	$emp = Employee::whereBarcode($request -> barcode)->first();

    	if (!TimeIn::where('emp_id', '=', $emp -> id)->whereDate('created_at',DB::raw('CURDATE()'))->exists()) {
    		$timein = new TimeIn;

    		$timein->emp_id = $emp -> id;

    		$timein->save();
    	}
    	elseif (!TimeOut::where('emp_id', '=', $emp -> id)->whereDate('created_at',DB::raw('CURDATE()'))->exists()) {
    		$timein = TimeIn::where('emp_id', '=', $emp -> id)->whereDate('created_at',DB::raw('CURDATE()'))->first();
			$timein->time_out = 1;
			$timein->save();

    		$timeout = new TimeOut;

    		$timeout->emp_id = $emp -> id;

    		$timeout->save();
    	}
    	else {
    		return redirect()->route('employee.index');
    	}

        return redirect()->route('time.index');
    }
}
