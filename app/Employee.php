<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    function time_ins(){
    	return $this->hasMany(TimeIn::class, 'time_ins');
    }
    function time_outs(){
    	return $this->hasMany(TimeOut::class, 'time_outs');
    }
}
