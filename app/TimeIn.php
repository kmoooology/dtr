<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeIn extends Model
{
    function employees(){
    	return $this->belongsTo(Employee::class, 'employees');
    }
}
